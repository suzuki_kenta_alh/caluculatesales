package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String NUMERICAL_OVERFLOW = "合計金額が10桁を超えました";
	private static final String NOT_CONSECUTIVE = "売上ファイルが連番になっていません";
	private static final String INCORRENT_FORMAT = "のフォーマットが不正です";
	private static final String INCORRENT_BRANCH_CODE = "の支店コードが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		//コマンドライン引数が渡されているか確認
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		BufferedReader br = null;

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)

		//すべてのファイルを取得
		File[] files = new File(args[0]).listFiles();

		String filesStr;
		List<File> rcdFiles = new ArrayList<File>();

		for (int i = 0; i < files.length; i++) {
			//売上ファイルか判定、売上ファイルならListに格納
			filesStr = files[i].getName();

			//売上ファイルがFileか確認
			//売上ファイルがのフォーマット確認
			if (files[i].isFile() && filesStr.matches("^[0-9]{8}.+rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//売上ファイルの連番確認
		Collections.sort(rcdFiles);

		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			String rcdStr = rcdFiles.get(i).getName();
			String rcdStrSc = rcdFiles.get(i + 1).getName();

			int former = Integer.parseInt(rcdStr.substring(0, 8));
			int latter = Integer.parseInt(rcdStrSc.substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println(NOT_CONSECUTIVE);
				return;
			}
		}

		//rcdFilesを1つずつ読み込んでいく
		for (int j = 0; j < rcdFiles.size(); j++) {
			try {
				File fileSale = rcdFiles.get(j);
				FileReader fr = new FileReader(fileSale);
				br = new BufferedReader(fr);
				List<String> rcdList = new ArrayList<String>();
				String line;

				// 一行ずつ読み込む
				while ((line = br.readLine()) != null) {
					rcdList.add(line);
				}

				//売上ファイルのフォーマット確認
				if (rcdList.size() != 2) {
					System.out.println(rcdFiles.get(j).getName() + "の" + INCORRENT_FORMAT);
					return;
				}

				//key(支店コード)の一致を確認
				if (!branchNames.containsKey(rcdList.get(0))) {
					System.out.println(rcdFiles.get(j).getName() + "の" + INCORRENT_BRANCH_CODE);
					return;
				}

				//売上金額が数字か確認
				if (!rcdList.get(1).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//rcdList[1](売上金額)をLong型に型変換
				Long rcdSale = Long.parseLong(rcdList.get(1));

				//saleAmountに合計金額を代入
				Long saleAmount = branchSales.get(rcdList.get(0)) + rcdSale;

				//合計金額の桁数確認
				if (saleAmount >= 10000000000L) {
					System.out.println(NUMERICAL_OVERFLOW);
					return;
				}

				//saleAmountをbranchSalesに代入
				branchSales.put(rcdList.get(0), saleAmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> branchNames,
			Map<String, Long> branchSales) {

		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//支店定義ファイルの存在確認
			if (!file.exists()) {
				System.out.println(FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)

				String[] fileDeta = line.split(","); //支店コードと支店名に分離

				//支店定義ファイルのフォーマット確認
				if ((fileDeta.length != 2) || (!fileDeta[0].matches("^[0-9]{3}$"))) {
					System.out.println(FILE_INVALID_FORMAT);
					return false;
				}

				branchNames.put(fileDeta[0], fileDeta[1]); //("支店コード","支店名")
				branchSales.put(fileDeta[0], 0L); //("支店コード","売上金額")
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();

				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames,
			Map<String, Long> branchSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		//File作成
		try {
			File file = new File(path, fileName);

			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//繰り返しkeyを取得
			for (String key : branchNames.keySet()) {
				//支店コード,支店名,売上金額となる
				String allDeta = key + "," + branchNames.get(key) + "," + branchSales.get(key);

				//書き込み処理
				bw.write(allDeta);
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
